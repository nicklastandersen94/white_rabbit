<?php

class WhiteRabbit2 {
	/**
	 * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
	 * The returned array should use as few coins as possible.
	 * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
	 * You can assume that $amount will be an int
	 */
	public function findCashPayment( $amount ) {

		$coinsArray = [
			'1'   => 0,
			'2'   => 0,
			'10'  => 0,
			'5'   => 0,
			'20'  => 0,
			'50'  => 0,
			'100' => 0
		];

		if ( $amount === 0 ) {
			// If amount is zero - we return the array - with it's already nulled value pairs.
			return $coinsArray;
		}

		// As our array is already sorted by default in the correct order - we can just "reverse" it - and preserve it's keys.
		// Using ksort to ensure that it's always sorted numeric and in ascending order.
		ksort( $coinsArray, SORT_NUMERIC );
		$coinsArray = array_reverse( $coinsArray, true );

		// Looping thru the coinsArray.
		foreach ( $coinsArray as $key => $value ) {
			// Checking if amount is bigger or equal to 1 - if it's correct - we proceed.
			if ( $amount > 0 ) {
				$value              = floor( $amount / $key );
				$coinsArray[ $key ] = $value;
				$amount             = $amount % $key;
			}
		}

		return $coinsArray;
	}
}
