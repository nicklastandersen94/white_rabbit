<?php

class WhiteRabbit {
	public function findMedianLetterInFile( $filePath ) {
		return [
			"letter" => $this->findMedianLetter( $this->parseFile( $filePath ), $occurrences ),
			"count"  => $occurrences
		];
	}

	/**
	 * Parse the input file for letters.
	 *
	 * @param $filePath
	 */
	private function parseFile( $filePath ) {
		//TODO implement this!
		// Getting contents of file

		// Getting the content of the file - and returning it.
		$file = file_get_contents( $filePath );

		return $file;
	}

	/**
	 * Return the letter whose occurrences are the median.
	 *
	 * @param $parsedFile
	 * @param $occurrences
	 */
	private function findMedianLetter( $parsedFile, &$occurrences ) {

		// Preg_replace used to filter out all characters and spaces not being a letter.
		$parsedFile = preg_replace( "/[^a-zA-Z]+/", "", $parsedFile );

		$allOccurrences = count_chars( strtolower( $parsedFile ), 1 );

		asort( $allOccurrences, SORT_NUMERIC );

		$median = (count( $allOccurrences ) ) / 2;

		$letter      = array_keys( $allOccurrences )[ $median - 1 ];
		$occurrences = $allOccurrences[ $letter ];

		return chr( $letter );
	}

}
